use std::rc::Rc;
use office::{Excel, DataType};
use crossterm::{cursor, execute, terminal, event};

use std::io::{self, Cursor};
use std::{env, thread, u64};
use std::collections::{ HashSet};
use std::path::PathBuf;
use std::time::Duration;
use crossterm::event::{Event, KeyCode, KeyEvent, KeyEventKind};
use crossterm::style::{Print,SetForegroundColor,Color};
use crossterm::terminal::ClearType;
use rand::seq::SliceRandom;
use rodio::{Decoder, OutputStream, Sink};

// type Row = (Rc<u64>,Rc<String>, Rc<String>);
macro_rules! my_exec {
    ($($args:expr),*) => {
        execute!(io::stdout(), $($args),*).unwrap();
    };
}

macro_rules! color_exec {
    ($color:expr, $($args:expr),*) => {
        execute!(
            io::stdout(),
            SetForegroundColor($color),
            $($args),*,
            SetForegroundColor(Color::Reset)
        ).unwrap();
    };
}

#[derive(Debug,Clone)]
struct Row {
    id: u64,
    dutch: Rc<String>,
    translation: String,
}

impl Row {
   fn new(id: u64, dutch: String, translation: String) -> Row {
       Row { id, dutch: Rc::new(dutch), translation }
   }

    fn fmt_header() -> String {
        format!("\r{:<6} {:<6} {:<25} {:<25} {:<25}", "Num", "Id", "Word", "Translation", "Incorrect\n")
    }

    fn fmt_show_row_answer(&self, index: usize, answer: Option<Rc<String>>) -> String {
        match answer {
            None => {
                format!("\r{:<6} {:<6} {:<25} {:<25}\n", index, self.id, self.dutch, self.translation)
            }
            Some(incorrect) => {
                format!("\r{:<6} {:<6} {:<25} {:<25} {:<25}\n", index, self.id, self.dutch, self.translation, incorrect)
            }
        }
    }

    fn fmt_hide_row_answer(&self, index: usize) -> String {
        format!("\r{:<6} {:<6} {:<25}", index, self.id, self.dutch)
    }

    fn fmt_audio_version(&self) -> Rc<String> {
        if self.dutch.contains("(de)") {
            return Rc::new(format!("de {}",self.dutch.clone().replace("(de)","")));
        }
        else if self.dutch.contains("(het)") {
            return Rc::new(format!("het {}",self.dutch.clone().replace("(het)","")));
        }
        Rc::clone(&self.dutch)
    }

}
fn get_path_to_xlsx_file() -> Result<PathBuf, String> {
    if let Ok(mut current_exe) = env::current_exe() {
        current_exe.pop();
        current_exe.push("woordenlijst-engels.xlsx");
        Ok(current_exe)
    } else {
        Err(format!("Failed to get the current executable path"))
    }
}

fn read_wordlist(path: PathBuf) -> Result<Rc<Vec<Row>>, String> {
    let table_name = "Table 1";
    if let Ok(mut workbook) = Excel::open(path.as_path()) {
        if let Ok(worksheet) = workbook.worksheet_range(table_name) {
            let wordlist:Vec<Row> = worksheet.rows().enumerate()
                .filter_map(move |(id,row)| {
                    match (row[0].clone(), row[1].clone()) {
                        (DataType::String(word), DataType::String(translation)) => {
                            Some(Row::new((id + 1) as u64, word, translation))
                        }
                        _ => None
                    }
                })
                .collect();
            return Ok(Rc::new(wordlist));
        }
        else {
            Err(format!("File opened but unable to get Table={:?}",table_name))
        }
    }
    else {
        Err(format!("File not found at {:?}",path))
    }
}

fn read_number_from_terminal() -> usize {
    println!("Choose a lesson 1-42");
    loop {
        let mut input= String::new();
        io::stdin().read_line(&mut input).expect("Failed to read line");
        match input.trim().parse::<usize>() {
            Ok(number) => {
                return number
            }
            Err(_) => {
                println!("Invalid input. Please enter a valid number.");
            }
        }
    }
}

fn get_lesson_range_and_name(number: usize) -> (usize,usize,String) {
    //removed 496
    let lessons : [usize;44] = [0,1,55,112,168,224,280,335,394,451,504,559,618, 669,726,778,830,883,
        937,991,1046,1101,1154,1208,1258,1307,1363,1419,1471,1518,1572,1620,1671,1721,1771,
        1828,1871,1914,1965, 2016,2065,2110, 2139, 2170];
    let names = [
        "",
        "Hoe heet je?",
        "Uit welk land kom je?",
        "Welke dag is het vandaag",
        "De Nederlandse les",
        "Mijn dag",
        "Boodschappen doen",
        "Stap voor stap leren",
        "Eet smakelijk!",
        "Hoe kom je naar de les?",
        "De vier seizoenen",
        "Hoe wonen jullie?",
        "Hoe kom ik daar?",
        "Een leuk weekend",
        "Familie",
        "Pakje bij de buren",
        "School",
        "Wie doet het huishouden",
        "De huisarts",
        "De bank",
        "De mooiste reis",
        "Een dagje uit",
        "De Nederlandse bevolking",
        "Op de helft",
        "Trouwen, samewonen of liever allen blijven?",
        "De kaart van Nederland",
        "De politie: (niet) m'n beste vriend!",
        "Feest vieren",
        "De Randstad",
        "Vrije tijd",
        "Werk",
        "Blijf gezond!",
        "Mooi Nederland",
        "De papieren",
        "Van republiek naar koninkrijk",
        "Galukkige kinderen",
        "De twaalf provincies",
        "De gemeente",
        "Een bete klimaat begint bij jezelf",
        "Politiek",
        "De tijden zijn veranderd",
        "Mijn leven in Nederland",
        "De laatste les",
    ];
    return (lessons[number], lessons[number+1], names[number].parse().unwrap())
}

fn get_shuffled_lesson_slice(wordlist: Rc<Vec<Row>>, start: usize, end: usize) -> Rc<Vec<Row>> {
    let mut rng = rand::thread_rng();
    let mut result = wordlist[(start - 1)..end-1].to_vec(); //copies here maybe change this for memory
    assert_eq!(result.len(),(end - start));
    assert_eq!(result.get(result.len()-1).unwrap().id, (end-1) as u64);
    result.shuffle(&mut rng);
    Rc::new(result)
}

fn play_audio(word: Rc<String>) -> Result<(),String> {
    // Remember to add the "blocking" feature in the Cargo.toml for reqwest
    let url = google_translate_tts::url(&**word, "nl");
    if let Ok(resp) = reqwest::blocking::get(url) {
        let cursor = Cursor::new(resp.bytes().unwrap()); // Adds Read and Seek to the bytes via Cursor
        let source = Decoder::new(cursor).unwrap(); // Decoder requires it's source to impl both Read and Seek
        let (_stream, stream_handle) = OutputStream::try_default().unwrap();
        let sink = Sink::try_new(&stream_handle).unwrap();
        sink.append(source);
        sink.sleep_until_end();
        Ok(())
    }
    else {
        Err(format!("Failed to retrieve audio file from google translate"))
    }
}

fn read_answer() -> Option<Rc<String>> {
    let mut word = String::new();
    loop {
        match event::read().expect("Failed to read event") {
            Event::Key(KeyEvent{code, kind, .. }) => {
                match kind {
                    KeyEventKind::Press => {
                        match code {
                            KeyCode::Char(c) => {
                                execute!(io::stdout(), Print(format!("{}",c))).unwrap();
                                word.push(c);
                            }
                            KeyCode::Enter => {
                                return Some(Rc::new(word));
                            }
                            KeyCode::Esc => {
                                return None;
                            }
                            KeyCode::Backspace => {
                                if word.pop().is_some() {
                                    execute!(io::stdout(),
                                cursor::MoveLeft(1), Print(" "),
                                cursor::MoveLeft(1)).unwrap();
                                }
                            }
                            _ => {}
                        }

                    }
                    _ => {}
                }
            }
            _ => {}
        }
    }
}

fn check_answer(input: &String, expected: &String) -> bool {
    // Ignore spaces and Caps
    // Assume second option possible in parenthesis or slash
    let mut answers = [String::new(),String::new()];
    let mut inside = false;
    for c in expected.chars() {
        match c {
            '(' => inside = true,
            '/' => inside = true,
            ')' => inside = false,
            _ if inside => answers[1].push(c),
            _ => answers[0].push(c)
        }
    }
    return !input.is_empty() && answers.iter().any(|s| s.trim().eq_ignore_ascii_case(input.trim()));
}

fn test_duplicates(slice: Rc<Vec<Row>>) {
    let set: HashSet<_> = slice.iter().map(|x| x.dutch.clone()).collect();
    if set.len() != slice.len() {
        println!("Duplicates");
        let mut seen = HashSet::new();
        for element in slice.iter() {
            if !seen.insert(element.dutch.clone()) {
                println!("\r{:?}", element);
            }
        }
    }
}

fn play_audio_iff_internet_connection(internet_connection: bool, index:usize, row: &Row) -> bool {
    if !internet_connection || play_audio(row.fmt_audio_version()).is_err() {
        if internet_connection {
            //lost internet connection for the first time inform user
            color_exec!(
                Color::Blue,
                crossterm::terminal::Clear(ClearType::CurrentLine),
                Print("\rNo Internet Connection... Turning off Text to Audio\n")
            );
            // reprint previous line
            my_exec!(
                Print(row.fmt_hide_row_answer(index + 1)),
                cursor::MoveRight(1)
            );
        }
        return false
    }
    true
}

fn game_loop() -> Result<(),String> {
    println!("STARTING EXECUTABLE");
    let mut internet_connection = true;
    let path = get_path_to_xlsx_file()?;
    let wordlist = read_wordlist(path)?;
    let number = read_number_from_terminal();
    let (start,end,name) = get_lesson_range_and_name(number);
    let slice= get_shuffled_lesson_slice(Rc::clone(&wordlist),start,end);
    println!("Lesson: {}\nRange=({:?},{:?}) total_words={:?}", name, start, end, slice.len());
    println!("{}",Row::fmt_header());

    terminal::enable_raw_mode().unwrap(); // allows fine tuning control of terminal don't use println while active
    let mut success_counter : f32 = 0.0;
    for (index,row) in slice.iter().enumerate() {
        my_exec!(
            Print(row.fmt_hide_row_answer(index + 1)),
            cursor::MoveRight(1)
        );
        internet_connection = play_audio_iff_internet_connection(internet_connection, index, row);
        if let Some(answer) = read_answer() {
            if check_answer(&answer, &row.translation) {
                success_counter +=1.0;
                color_exec!(Color::Green,Print(row.fmt_show_row_answer(index+1,None)));
            } else {
                color_exec!(Color::Red,Print(row.fmt_show_row_answer(index+1,Some(answer)))
                );
            };
        }
        else {
            my_exec!(Print("\r\n\nESC detected, Shutting down\n\r"));
            return Ok(());
        }
    }
    success_counter = (100.0 * success_counter) / slice.len() as f32;
    my_exec!(Print(format!("\r\n\nSuccess rate={}%\n\r",success_counter)));
    thread::sleep(Duration::from_secs(3));
    Ok(())
}

fn main() {
    // 1) Choose Lessons and do them to completion (done)
    // 2) Add audio from google translate (done)
    // 3) Compile for Linux easily (done)
    // 4) Rename executable to something else (done)
    // 5) Compile for windows 10 (done)
    // 6) Able to enter correct answer in the keyboard (done)
    // 7) Fix cursor not aligned and backspace should not delete previous words (done)
    // 8) Make answers less strict (case insensitive, multiple answers accept one as correct) (done)
    // 9) Show wrong answer (done)
    // 10) Removed duplicates per lesson (1,2,9,12) not entirety (done)
    // 11) Add some time when the script completes like 3 seconds (done)
    // 12) Show names of lesson (done)
    // 13) Show success percentage (done)
    // 14) Check to see if Internet Connection available and work offline (done)
    // 15) Created my own macros to simplify code (done)
    // 16) Create custom struct for rows with pretty printing (done)
    // 17) If the second word is a de/het then say it first (done)
    // 18) empty strings should not be correct answers (done)
    // 19) add an icon (maybe a dutch flag)
    // 20) add a mode where words are randomly picked from all lessons
    // 21) Able to switch from nl->eng to eng->nl at start
    // 22) Add image support blocking for now(see github issue that I raised for image_search)
    // 23) Turn into async for better response (input,audio and images downloading can be made async)
    // 24) Record which ones I get wrong and show them at the end (no memory)
    // 25) Keep memory by overwriting excel file with my score
    match game_loop() {
        Ok(_) => {}
        Err(e) => {
                print!("Caught error: {}", e);
        }
    }
    terminal::disable_raw_mode().unwrap();
}
